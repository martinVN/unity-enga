﻿using UnityEngine;
using UnityEngine.UI;

public class UIInputHandler : MonoBehaviour
{
    private GameMaster gameMaster;
    public Text scoreText;

    private void Start()
    {
        gameMaster = GameMaster.instance;
    }

    private void Update()
    {
        scoreText.text = "Points:" + GameMaster.instance.points.ToString();
    }



    public void OnRestartClick()
    {
        gameMaster.RestartGame();
    }

    public void OnQuitClick()
    {
        gameMaster.QuitGame();
    }
}