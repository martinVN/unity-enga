﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StyreBall : MonoBehaviour {

    public float speed = 10;

    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void FixedUpdate () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(-vertical, 0, horizontal) * speed * Time.deltaTime;
        //print(horizontal);
        //GetComponent<Rigidbody>().AddForce(movement, ForceMode.VelocityChange);
        GetComponent<Rigidbody>().velocity += movement;
        //GetComponent<Rigidbody>().MovePosition(transform.position + movement);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Danger")
        {
            GameMaster.instance.RestartGame();
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //print("Er borti den røde kuben");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Point")
        {
            print("Got a point");
            GameMaster.instance.AddPoints(1);
            Destroy(other.gameObject);
        }else if (other.gameObject.tag == "NextLevel")
        {
            GameMaster.instance.LoadNextLevel();
        }
    }




}
