﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchInput : MonoBehaviour {

    public float maxDragDistance = 400;
    public float deadZoneDistance = 50f;
    public float speed = 10f;

    static private readonly Vector2 invalidScreenPoint = -Vector2.one;

    //public Text debugText;

    Vector2 TouchOrigin = invalidScreenPoint;
    Vector2 DeltaTouch = invalidScreenPoint;
    // Use this for initialization
    void Awake ()
    {
        ResetTouchVariables();
    }

    private void ResetTouchVariables()
    {
        TouchOrigin = invalidScreenPoint;
        DeltaTouch = invalidScreenPoint;
    }


    // Update is called once per frame
    void Update () {
        if (Input.touchCount > 0)
        {
            SetTouchValues();
            SetMovement();
        }
    }

    private void SetMovement()
    {
        float vertical = 0f;
        float horizontal = 0f;
        if (Mathf.Abs(DeltaTouch.y) > deadZoneDistance)
        {
            vertical = Mathf.Clamp(DeltaTouch.y, -maxDragDistance, maxDragDistance) / maxDragDistance * Time.deltaTime * speed;

        }
        if (Mathf.Abs(DeltaTouch.x) > deadZoneDistance)
        {
            horizontal = Mathf.Clamp(DeltaTouch.x, -maxDragDistance, maxDragDistance) / maxDragDistance * Time.deltaTime * speed;

        }
        Vector3 movement = new Vector3(-vertical, 0, horizontal) * speed * Time.deltaTime;

        //topText.text = "Delta x: " + DeltaTouch.x;
        //GetComponent<Rigidbody>().AddForce(movement, ForceMode.VelocityChange);
        GetComponent<Rigidbody>().velocity += movement;
    }

    private void SetTouchValues()
    {
        Touch touch = Input.GetTouch(0);
        switch (touch.phase)
        {
            case TouchPhase.Began:
                TouchOrigin = touch.position;
                break;
            case TouchPhase.Moved:
            case TouchPhase.Stationary:
                if (TouchOrigin != invalidScreenPoint)
                {
                    DeltaTouch = touch.position - TouchOrigin;
                }

                break;
            case TouchPhase.Ended:
            case TouchPhase.Canceled:
                ResetTouchVariables();
                break;
            default:
                break;
        }
    }
}
